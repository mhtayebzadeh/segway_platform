#include <MPU9250_asukiaaa.h>


#include <DynamixelWorkbench.h>

#if defined(__OPENCM904__)
  #define DEVICE_NAME "1" //Dynamixel on Serial1(USART1)  <-OpenCM9
#elif defined(__OPENCR__)
  #define DEVICE_NAME ""
#endif   

#define BAUDRATE  1000000
#define DXL_ID    3

#include "math.h"
MPU9250_asukiaaa mySensor;
#define sampleTime  0.0045

#define alpha 0.95

#define ANGLE_SEND_PACKET_ID 1
#define ANGULAR_VELOCITY_SEND_PACKET_ID 2

int32_t motor_speed=200;
int32_t l_motor_speed=0 , r_motor_speed=0; l_motor_torque = 0 ; r_motor_torque=0;

int u;

uint8_t dxl_id_left = 3;
uint8_t dxl_id_right = 4 , dxl_id = 4;
uint16_t model_number = 0;



float gX, gY, gZ,gyroAngle,accAngle,gyroRate;
float aX, aY, aZ, aSqrt;
unsigned long currTime, prevTime=0, loopTime;
float currentAngle, prevAngle=0;
long sampleTime_us = (long)(sampleTime * 1000000.0);
long lastTime;
int loopNum=0;
////////

DynamixelWorkbench dxl_wb;


uint8_t inp_vec[7] = {0,0,0,0,0,0,0}; 


void setup() 
{  
  Serial.begin(115200);
  // while(!Serial); // Wait for Opening Serial Monitor
  Serial2.begin(115200);
  const char *log;
  bool result = false;

  result = dxl_wb.init(DEVICE_NAME, BAUDRATE, &log);
  if (result == false)
  {
    Serial.println(log);
    Serial.println("Failed to init");
  }
  else
  {
    Serial.print("Succeeded to init : ");
    Serial.println(BAUDRATE);  
  }

  result = dxl_wb.ping(dxl_id_left, &model_number, &log);
  if (result == false)
  {
    Serial.println(log);
    Serial.println("Failed to ping dxl_id_left");
  }
  else
  {
    Serial.println("Succeeded to ping dxl_id_left");
    Serial.print("id : ");
    Serial.print(dxl_id_left);
    Serial.print(" model_number : ");
    Serial.println(model_number);
  }

  result = dxl_wb.ping(dxl_id_right, &model_number, &log);
  if (result == false)
  {
    Serial.println(log);
    Serial.println("Failed to ping dxl_id_right");
  }
  else
  {
    Serial.println("Succeeded to ping dxl_id_right");
    Serial.print("id : ");
    Serial.print(dxl_id_right);
    Serial.print(" model_number : ");
    Serial.println(model_number);
  }
  
  result = dxl_wb.wheelMode(dxl_id_right, 0, &log);
  if (result == false)
  {
    Serial.println(log);
    Serial.println("Failed to change wheel mode dxl_id_right");
  }
  else
  {
    Serial.println("Succeed to change wheel mode dxl_id_right");
    Serial.println("Dynamixel is moving...");
  }
  result = dxl_wb.wheelMode(dxl_id_left, 0, &log);
  if (result == false)
  {
    Serial.println(log);
    Serial.println("Failed to change wheel mode dxl_id_left");
  }
  else
  {
    Serial.println("Succeed to change wheel mode dxl_id_left");
    Serial.println("Dynamixel is moving...");
  }


  Wire.begin();
  mySensor.setWire(&Wire);
  mySensor.beginAccel();
  mySensor.beginGyro();
  mySensor.beginMag();

  pinMode(7 , OUTPUT);
  
  lastTime = micros();
}

void loop() 
{
  mySensor.accelUpdate();
  aX = mySensor.accelX();
  aY = mySensor.accelY();
  aZ = mySensor.accelZ();
  aSqrt = mySensor.accelSqrt();

  mySensor.gyroUpdate();
  gX = mySensor.gyroX();
  gY = mySensor.gyroY();
  gZ = mySensor.gyroZ();

  accAngle = atan2(aY, aZ)*RAD_TO_DEG;
  
  gyroRate = map(gX, -32768, 32767, -250, 250);
  gyroAngle = (float)gyroRate*sampleTime;  
//  currentAngle = 0.9934*(currentAngle + gyroAngle) + 0.0066*(accAngle);
  currentAngle = alpha*(currentAngle + gyroAngle) + (1.0-alpha)*(accAngle);
  prevAngle = currentAngle;
//  Serial.println(currentAngle);
  loopNum++;
  if(loopNum > 3)
  {
    loopNum = 0;
    send_packet(ANGLE_SEND_PACKET_ID , currentAngle);
    send_packet(ANGULAR_VELOCITY_SEND_PACKET_ID , gyroRate);
    setMotorVelocity();
  }
  
  while( abs(micros() - lastTime) < sampleTime_us){
//    Serial.println("++++++++++++++++");
    }
  lastTime = micros();

}

void setMotorVelocity()
{
  dxl_wb.goalVelocity((uint8_t)dxl_id_left, (int32_t)l_motor_speed);
  dxl_wb.goalVelocity((uint8_t)dxl_id_right, (int32_t)r_motor_speed);
}


void setMotorTorque()
{
  dxl_wb.goalVelocity((uint8_t)dxl_id_left, (int32_t)l_motor_speed);
  dxl_wb.goalVelocity((uint8_t)dxl_id_right, (int32_t)r_motor_speed);
}

void send_packet(byte id ,float arg){
  byte* current_angle_byte_array = (byte *)& arg;
  byte prefix = 0x00;
  byte postfix = 0xFF;
//  byte id = 1;/
  Serial.write(prefix);
  Serial.write(prefix);
  Serial.write(id);
  Serial.write(current_angle_byte_array, sizeof(arg));
  Serial.write(postfix);
}

void serialEvent() {

  while (Serial.available()) {
    // get the new byte:
    uint8_t inChar = (uint8_t)Serial.read();
    inp_vec[0] = inp_vec[1]; 
    inp_vec[1] = inp_vec[2];
    inp_vec[2] = inp_vec[3]; 
    inp_vec[3] = inp_vec[4];
    inp_vec[4] = inp_vec[5]; 
    inp_vec[5] = inp_vec[6];
    inp_vec[6] = inChar;
    if (inp_vec[0]==0 && inp_vec[1]==0 && inp_vec[6]==255)
    {

      // speed control
//      l_motor_speed = (int32_t)((int16_t)(inp_vec[3]<<8 | inp_vec[2]));
//      r_motor_speed = -(int32_t)((int16_t)(inp_vec[5]<<8 | inp_vec[4]));
//      l_motor_speed = constrain(l_motor_speed , -1023 , 1023);
//      r_motor_speed = constrain(r_motor_speed , -1023 , 1023);

      // torque control
      l_motor_torque = (int32_t)((int16_t)(inp_vec[3]<<8 | inp_vec[2]));
      r_motor_torque = -(int32_t)((int16_t)(inp_vec[5]<<8 | inp_vec[4]));
      l_motor_torque = constrain(l_motor_speed , -1023 , 1023);
      r_motor_torque = constrain(r_motor_speed , -1023 , 1023);

      Serial.flush();
    }
  
  }
  

  
}
