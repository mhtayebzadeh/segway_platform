#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float32
import sys
import serial
import struct
import time
import datetime
import threading , _thread
import os
from serial.tools import list_ports

try:
    availablePortsList = list_ports.comports()
    serialPort = availablePortsList[0][0]
except:
    print("Exception : No serial port founded ...")
    os._exit(1)

angle1 = 0.1
angle2 = 0.1
angle3 = 0.1
angle4 = 0.1

writeSpeedFlag = 0;
motorSpeed1 = 0
motorSpeed2 = 0

motor1_id = 3 ## left motor
motor2_id = 4 ## right motor
leftMotorDirection = 1
rightMotorDirection = 1
minSpeed = 0
maxSpeed = 1023
inputVelMax = 100

period_in_micro = 20000 ## us

def sendReceiveSerialPacket_handler(port , baud , timeout_):
    try:
        sendReceiveSerialPacket(port , baud , timeout_)
    except:
        print("Exception in serial connection ...")
        os._exit(1)


def sendReceiveSerialPacket(port , baud , timeout_):
    buff = []
    n = 0;
    TotalPacketSize = 8
    t_init1 = datetime.datetime.now()
    global angle1
    global angle2
    global motorSpeed1
    global motorSpeed2
    global writeSpeedFlag
    t1 , t2 , t3 , t4 , t5 = angle1 ,angle2 , motorSpeed1 , motorSpeed2 , writeSpeedFlag
    tmp_angle1 = []
    sum_dt1 = 0
    with serial.Serial(port, baud , timeout = timeout_) as ser:
        while 1:
            # print(ser.in_waiting)
            x = ser.read(ser.in_waiting)
            time.sleep(0.003)
            x = list(x)
            # print(x)
            buff = buff + x
            while len(buff) >= TotalPacketSize:
                
                if buff[0] == 0x00 and buff[1] == 0x00 and buff[TotalPacketSize - 1] == 0xff:
                    n += 1
                    s = bytes(buff[3:TotalPacketSize-1]) ;
                    a = struct.unpack('f' , s)[0]
                    sensorId = int(buff[2])

                    if sensorId == 1:
                        dt1 = (datetime.datetime.now() - t_init1).microseconds; ## us
                        t_init1 = datetime.datetime.now()
                        sum_dt1 += dt1;
                        if sum_dt1 >= period_in_micro:
                            # print(angle1)
                            tmp_angle1 = []
                            sum_dt1 = 0
                        tmp_angle1.append(a)
                        lenght = len(tmp_angle1);
                        if lenght != 0:
                            angle1 = sum(tmp_angle1)/lenght;



                buff.pop(0)

            if writeSpeedFlag == 1:
                ## send motor speed
                #TODO : this packet for segway designed 
                bb = [0,0 , motorSpeed1 & 0xff, motorSpeed1>>8 & 0xff , motorSpeed2 & 0xff,  motorSpeed2>>8 & 0xff  ,255]
                print(bb)
                # exec(0)
                # ser.flush()                
                ser.write(bytearray(bb))
                # ser.flush()                
                # ser.write()
                writeSpeedFlag = 0


def readAngle(sensorId):
    offsetAngle1 = rospy.get_param("offsetAngle1")    
    sensors = [angle1+offsetAngle1 , angle2 , angle3 , angle4 ,]
    return sensors[sensorId-1]

def callback_linVel(data):
    # -100 < input velocity < 100
    global motorSpeed1
    global motorSpeed2
    global writeSpeedFlag

    s = data.data
    if(s>inputVelMax):
        s = inputVelMax
    elif(s<-inputVelMax):
        s = -inputVelMax
    s = int(s * (maxSpeed - minSpeed)/inputVelMax);
    motorSpeed1 = s*leftMotorDirection
    motorSpeed2 = s*rightMotorDirection
    writeSpeedFlag = 1
    
def callback_motor1(data): # left motor
    # -100 < input velocity < 100
    global motorSpeed1
    global writeSpeedFlag
    s = data.data
    if(s>inputVelMax):
        s = inputVelMax
    elif(s<-inputVelMax):
        s = -inputVelMax
    s = int(s * (maxSpeed - minSpeed)/inputVelMax);
    motorSpeed1 = s*leftMotorDirection
    writeSpeedFlag = 1

def callback_motor2(data): # right motor
    # -100 < input velocity < 100
    global motorSpeed2
    global writeSpeedFlag
    s = data.data
    if(s>inputVelMax):
        s = inputVelMax
    elif(s<-inputVelMax):
        s = -inputVelMax
    s = int(s * (maxSpeed - minSpeed)/inputVelMax);
    motorSpeed2 = s*rightMotorDirection
    writeSpeedFlag = 1

def listener():  
    rospy.Subscriber("lin_vel", Float32, callback_linVel)
    rospy.Subscriber("motor1/vel", Float32, callback_motor1)
    rospy.Subscriber("motor2/vel", Float32, callback_motor2)
    # exit(1)
    rospy.spin()

def myControler(theta):
    global motorSpeed1
    global motorSpeed2
    global writeSpeedFlag
    desire = 0.0
    err = desire - theta
    kp = rospy.get_param("Kp") 
    ki = rospy.get_param("Ki") 
    kd = rospy.get_param("Kd") 
    c = kp*err ;
    motorSpeed1 = int(c)
    motorSpeed2 = int(c)
    writeSpeedFlag = 1

if __name__=="__main__":    
    try:
        rospy.init_node('arduino_segway_interface', anonymous=True)
        pub_imu1 = rospy.Publisher('sensor1/angle', Float32, queue_size=2)
        pub_imu2 = rospy.Publisher('sensor2/angle', Float32, queue_size=2)
        pub_imu3 = rospy.Publisher('sensor3/angle', Float32, queue_size=2)
        ros_sub_thread = threading.Thread(target=listener , daemon=False)
        ros_sub_thread.start()
        rate = rospy.Rate(50) # 50hz
        try:
            offsetAngle1 = rospy.get_param("offsetAngle1")
        except:
            rospy.set_param("offsetAngle1"  , 0.0)
        
        try:
            kp = rospy.get_param("Kp") 
            ki = rospy.get_param("Ki") 
            kd = rospy.get_param("Kd") 
        except:
            rospy.set_param("Kp"  , 1.0)
            rospy.set_param("Ki"  , 0.0)
            rospy.set_param("Kd"  , 0.0)
            
    except:
        print("Exception : roscore not working")
        os._exit(1)



    print('port = ' + serialPort)
    time.sleep(1)  
    serial_receive_thread = threading.Thread(target=sendReceiveSerialPacket_handler , args=(serialPort , 115200 , 1) , daemon=False )
    serial_receive_thread.start()

    ros_sub_thread = threading.Thread(target=listener , daemon=False)
    ros_sub_thread.start()
    

    print("start loop")
    while  not rospy.is_shutdown():
        pub_imu1.publish(readAngle(sensorId = 1))
        print(readAngle(sensorId = 1))
        # myControler(readAngle(sensorId = 1))  ##  optional ..
        rate.sleep()

    print("ROS Node interrupt ...")
    os._exit(1)


