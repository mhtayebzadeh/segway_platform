#include <DynamixelSDK.h>

// Control table address could be differ by Dynamixel Series
#define ADDRESS_TORQUE_ENABLE           64
#define ADDRESS_GOAL_CURRENT           102
#define ADDRESS_GOAL_SPEED             104
#define ADDRESS_PRESENT_POSITION        132


// Data Byte Length
#define LENGTH_GOAL_CURRENT            2
#define LENGTH_GOAL_SPEED              4
#define LENGTH_PRESENT_POSITION         4

// Protocol version
#define PROTOCOL_VERSION                2.0                 // See which protocol version is used in the Dynamixel

// Default setting
#define DXL1_ID                         1                   // Dynamixel#1 ID: 1
#define DXL2_ID                         2                   // Dynamixel#2 ID: 2
#define BAUDRATE                        1000000
#define DEVICENAME                      "1"                 // Check which port is being used on your controller
// DEVICENAME "1" -> Serial1
// DEVICENAME "2" -> Serial2
// DEVICENAME "3" -> Serial3(OpenCM 485 EXP)

#define TORQUE_ENABLE                   1                   // Value for enabling the torque
#define TORQUE_DISABLE                  0                   // Value for disabling the torque
#define DXL_MINIMUM_POSITION_VALUE      100                 // Dynamixel will rotate between this value
#define DXL_MAXIMUM_POSITION_VALUE      4000                // and this value (note that the Dynamixel would not move when the position value is out of movable range. Check e-manual about the range of the Dynamixel you use.)
#define DXL_MOVING_STATUS_THRESHOLD     20                  // Dynamixel moving status threshold

#define ESC_ASCII_VALUE                 0x1b



int dxl_comm_result = COMM_TX_FAIL;             // Communication result
bool dxl_addparam_result = false;                // addParam result
bool dxl_getdata_result = false;                 // GetParam result

uint8_t dxl_error = 0;                          // Dynamixel error
int32_t dxl1_present_position = 0, dxl2_present_position = 0;              // Present position



dynamixel::PortHandler *portHandler;

dynamixel::PacketHandler *packetHandler;

dynamixel::GroupSyncWrite *groupSyncWrite;

dynamixel::GroupSyncRead *groupSyncRead;



#define GY995_SERIAL Serial2

unsigned char Re_buf[30], counter = 0;
unsigned char sign = 0;
long int micro;
float ROLL, PITCH, YAW;
float Q4[4];


#if defined(__OPENCM904__)
#define DEVICE_NAME "1" //Dynamixel on Serial1(USART1)  <-OpenCM9
#elif defined(__OPENCR__)
#define DEVICE_NAME ""
#endif

#include "math.h"

#define sampleTime  0.0045

#define alpha 0.95

#define ANGLE_SEND_PACKET_ID 1
#define ANGULAR_VELOCITY_SEND_PACKET_ID 2

int32_t motor_speed = 200;
int32_t l_motor_current = 0 , r_motor_current = 0;
int32_t l_motor_speed = 0 , r_motor_speed = 0;
int u;

uint8_t dxl_id_left = 3;
uint8_t dxl_id_right = 4 , dxl_id = 4;
uint16_t model_number = 0;

float gX, gY, gZ, gyroAngle, accAngle, gyroRate;
float aX, aY, aZ, aSqrt;
unsigned long currTime, prevTime = 0, loopTime;
float currentAngle, prevAngle = 0;
long sampleTime_us = (long)(sampleTime * 1000000.0);
long lastTime;
int newCommand = 0;
int loopNum = 0;
////////
uint8_t inp_vec[7] = {0, 0, 0, 0, 0, 0, 0};


void init_current_mode()
{

  // Enable Dynamixel#1 current mode
  dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL1_ID, 11, 0, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    packetHandler->getTxRxResult(dxl_comm_result);
  }
  else if (dxl_error != 0)
  {
    packetHandler->getRxPacketError(dxl_error);
  }

  // Enable Dynamixel#2 current mode
  dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL2_ID, 11, 0, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    packetHandler->getTxRxResult(dxl_comm_result);
  }
  else if (dxl_error != 0)
  {
    packetHandler->getRxPacketError(dxl_error);
  }

  // Initialize GroupSyncWrite instance
  groupSyncWrite = new dynamixel::GroupSyncWrite(portHandler, packetHandler, ADDRESS_GOAL_CURRENT, LENGTH_GOAL_CURRENT);
}

void init_speed_mode()
{

  // Enable Dynamixel#1 current mode
  dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL1_ID, 11, 1, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    packetHandler->getTxRxResult(dxl_comm_result);
  }
  else if (dxl_error != 0)
  {
    packetHandler->getRxPacketError(dxl_error);
  }

  // Enable Dynamixel#2 current mode
  dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL2_ID, 11, 1, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    packetHandler->getTxRxResult(dxl_comm_result);
  }
  else if (dxl_error != 0)
  {
    packetHandler->getRxPacketError(dxl_error);
  }

  // Initialize GroupSyncWrite instance
  groupSyncWrite = new dynamixel::GroupSyncWrite(portHandler, packetHandler, ADDRESS_GOAL_SPEED, LENGTH_GOAL_SPEED);
}

void setup() {
  GY995_SERIAL.begin(9600);

  GY995_SERIAL.write(0xAA);
  GY995_SERIAL.write(0x38);    //初始化,连续输出模式
  GY995_SERIAL.write(0xE2);    //初始化,连续输出模式


  const char *log;
  bool result = false;

  pinMode(7 , OUTPUT);

  lastTime = micros();

  // Use UART port of DYNAMIXEL Shield to debug.
  Serial.begin(115200);

  // Initialize PortHandler instance
  // Set the port path
  // Get methods and members of PortHandlerLinux or PortHandlerWindows
  portHandler = dynamixel::PortHandler::getPortHandler(DEVICENAME);

  // Initialize PacketHandler instance
  // Set the protocol version
  // Get methods and members of Protocol1PacketHandler or Protocol2PacketHandler
  packetHandler = dynamixel::PacketHandler::getPacketHandler(PROTOCOL_VERSION);


  // Initialize Groupsyncread instance for Present Position
  groupSyncRead = new dynamixel::GroupSyncRead(portHandler, packetHandler, ADDRESS_PRESENT_POSITION, LENGTH_PRESENT_POSITION);

  // Open port
  if (portHandler->openPort())
  {
    Serial.print("Succeeded to open the port!\n");
  }
  else
  {
    Serial.print("Failed to open the port!\n");
    Serial.print("Press any key to terminate...\n");
    return;
  }

  // Set port baudrate
  if (portHandler->setBaudRate(BAUDRATE))
  {
    Serial.print("Succeeded to change the baudrate!\n");
  }
  else
  {
    Serial.print("Failed to change the baudrate!\n");
    Serial.print("Press any key to terminate...\n");
    return;
  }
  //// initial current mode control
  //init_current_mode();

  //// initial speed mode control
  init_speed_mode();

  // Enable Dynamixel#1 Torque
  dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL1_ID, ADDRESS_TORQUE_ENABLE, TORQUE_ENABLE, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    packetHandler->getTxRxResult(dxl_comm_result);
  }
  else if (dxl_error != 0)
  {
    packetHandler->getRxPacketError(dxl_error);
  }

  // Enable Dynamixel#2 Torque
  dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL2_ID, ADDRESS_TORQUE_ENABLE, TORQUE_ENABLE, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    packetHandler->getTxRxResult(dxl_comm_result);
  }
  else if (dxl_error != 0)
  {
    packetHandler->getRxPacketError(dxl_error);
  }

  // Add parameter storage for Dynamixel#1 present position value
  dxl_addparam_result = groupSyncRead->addParam(DXL1_ID);

  // Add parameter storage for Dynamixel#2 present position value
  dxl_addparam_result = groupSyncRead->addParam(DXL2_ID);


}


void loop() {
  read_rpy();
  currentAngle = ROLL;
  send_packet(1 , currentAngle);
  if (newCommand == 1)
  {
    //setCurrent(l_motor_current , r_motor_current);
    set_speed(l_motor_speed , r_motor_speed);
    
    newCommand = 0;
  }
  
  while (abs(micros() - micro) < 25000);
  micro = micros();
  //Serial.println(currentAngle);
}

void send_packet(byte id , float arg) {
  byte* current_angle_byte_array = (byte *)& arg;
  byte prefix = 0x00;
  byte postfix = 0xFF;
  //  byte id = 1;/
  Serial.write(prefix);
  Serial.write(prefix);
  Serial.write(id);
  Serial.write(current_angle_byte_array, sizeof(arg));
  Serial.write(postfix);
}

void serialEvent() {

  while (Serial.available()) {
    // get the new byte:
    uint8_t inChar = (uint8_t)Serial.read();
    inp_vec[0] = inp_vec[1];
    inp_vec[1] = inp_vec[2];
    inp_vec[2] = inp_vec[3];
    inp_vec[3] = inp_vec[4];
    inp_vec[4] = inp_vec[5];
    inp_vec[5] = inp_vec[6];
    inp_vec[6] = inChar;
    if (inp_vec[0] == 0 && inp_vec[1] == 0 && inp_vec[6] == 255)
    {

      //     speed control
      l_motor_speed = (int32_t)((int16_t)(inp_vec[3] << 8 | inp_vec[2]));
      r_motor_speed = -(int32_t)((int16_t)(inp_vec[5] << 8 | inp_vec[4]));
      l_motor_speed = constrain(l_motor_speed , -1023 , 1023);
      r_motor_speed = constrain(r_motor_speed , -1023 , 1023);

      // torque control
      //      l_motor_current = (int)((int16_t)(inp_vec[3]<<8 | inp_vec[2]));
      //      r_motor_current = -(int)((int16_t)(inp_vec[5]<<8 | inp_vec[4]));
      //      l_motor_current = constrain(l_motor_current , -1023 , 1023);
      //      r_motor_current = constrain(r_motor_current , -1023 , 1023);

      newCommand = 1;

      Serial.flush();
    }

  }



}

void serialEvent_gy955() {

  while (GY995_SERIAL.available()) {
    Re_buf[counter] = (unsigned char)GY995_SERIAL.read();
    if (counter == 0 && Re_buf[0] != 0x5A)
      return;      // 检查帧头

    counter++;
    if (counter == 20)             //接收到数据
    {
      counter = 0;               //重新赋值，准备下一帧数据的接收
      sign = 1;
    }
  }
}

void read_rpy() {
  unsigned char i = 0, sum = 0;
  int16_t DATA[7];
  serialEvent_gy955();
  if (sign)
  {
    // Serial.print('h');
    // for(i=0;i<19;i++)
    // sum+=Re_buf[i];
    // if(sum==Re_buf[i] )        //检查帧头，帧尾
    // {
    DATA[0] = (Re_buf[4] << 8) | Re_buf[5];
    DATA[1] = (Re_buf[6] << 8) | Re_buf[7];
    DATA[2] = (Re_buf[8] << 8) | Re_buf[9];
    /*DATA[3]=(Re_buf[10]<<8)|Re_buf[11];
      DATA[4]=(Re_buf[12]<<8)|Re_buf[13];
      DATA[5]=(Re_buf[14]<<8)|Re_buf[15];
      DATA[6]=(Re_buf[16]<<8)|Re_buf[17];*/
    YAW = (float)((uint16_t)DATA[0]) / 100;
    ROLL = (float)DATA[1] / 100;
    PITCH =  (float)DATA[2] / 100;

    //         float f1 = (float)((uint16_t)DATA[3]);
    //         float f2 = (float)((uint16_t)DATA[4]);
    //         float f3 = (float)((uint16_t)DATA[5]);
    //         float f4 = (float)((uint16_t)DATA[6]);
    //
    //         Serial.print(DATA[3]);
    //         Serial.print(",  ");
    //         Serial.print(DATA[4]);
    //         Serial.print(",  ");
    //         Serial.print(f3);
    //         Serial.print(" ,  ");
    //         Serial.print(f4);
    //         Serial.print(" ,  ");

    /* Q4[0]= (float)DATA[3]/10000;
      Q4[1]= (float)DATA[4]/10000;
      Q4[2]= (float)DATA[5]/10000;
      Q4[3]= (float)DATA[6]/10000;*/
    //Serial.print("RPY: ");
    //delay(50);
    /* apc.print( ROLL);
      apc.print(",  ");
      apc.print( PITCH);
      apc.print(",  ");
      apc.println( YAW);*/
    /*Serial.print("Q4: ");
      Serial.print( Q4[0]);
      Serial.print(",");
      Serial.print( Q4[1]);
      Serial.print(",");
      Serial.print( Q4[2]);
      Serial.print(",");
      Serial.print( Q4[3]);
      Serial.print(";");*/
    sign = 0;
    //}

    /*
         Serial.println();
         Serial.println();
         Serial.print( ROLL);
         Serial.print(",  ");
         Serial.print( PITCH);
         Serial.print(",  ");
         Serial.print( YAW);
         Serial.print(" ,  ");
       //  Serial.println();
    */


  }


}


void setCurrent(int c1 , int c2)
{
  uint8_t param_goal_current1[2];
  param_goal_current1[0] = DXL_LOBYTE(c1);
  param_goal_current1[1] = DXL_HIBYTE(c1);

  uint8_t param_goal_current2[2];
  param_goal_current2[0] = DXL_LOBYTE(c2);
  param_goal_current2[1] = DXL_HIBYTE(c2);

  dxl_addparam_result = groupSyncWrite->addParam(DXL1_ID, param_goal_current1);

  dxl_addparam_result = groupSyncWrite->addParam(DXL2_ID, param_goal_current2);

  // Syncwrite goal current
  dxl_comm_result = groupSyncWrite->txPacket();
  if (dxl_comm_result != COMM_SUCCESS) packetHandler->getTxRxResult(dxl_comm_result);

  // Clear syncwrite parameter storage
  groupSyncWrite->clearParam();
}


void set_speed(int32_t s1 , int32_t s2)
{

  s1 = (int32_t)((double)s1*210.0/1023.0);
  s2 = (int32_t)((double)s2*210.0/1023.0);
  uint8_t param_goal_speed1[4];

  param_goal_speed1[0] = DXL_LOBYTE(DXL_LOWORD(s1));
  param_goal_speed1[1] = DXL_HIBYTE(DXL_LOWORD(s1));
  param_goal_speed1[2] = DXL_LOBYTE(DXL_HIWORD(s1));
  param_goal_speed1[3] = DXL_HIBYTE(DXL_HIWORD(s1));

  uint8_t param_goal_speed2[4];

  param_goal_speed2[0] = DXL_LOBYTE(DXL_LOWORD(s2));
  param_goal_speed2[1] = DXL_HIBYTE(DXL_LOWORD(s2));
  param_goal_speed2[2] = DXL_LOBYTE(DXL_HIWORD(s2));
  param_goal_speed2[3] = DXL_HIBYTE(DXL_HIWORD(s2));

  dxl_addparam_result = groupSyncWrite->addParam(DXL1_ID, param_goal_speed1);

  dxl_addparam_result = groupSyncWrite->addParam(DXL2_ID, param_goal_speed2);

  // Syncwrite goal current
  dxl_comm_result = groupSyncWrite->txPacket();
  if (dxl_comm_result != COMM_SUCCESS) packetHandler->getTxRxResult(dxl_comm_result);

  // Clear syncwrite parameter storage
  groupSyncWrite->clearParam();
}

void readPosition()
{
  // Syncread present position
  dxl_comm_result = groupSyncRead->txRxPacket();
  if (dxl_comm_result != COMM_SUCCESS) packetHandler->getTxRxResult(dxl_comm_result);

  // Check if groupsyncread data of Dynamixel#1 is available
  dxl_getdata_result = groupSyncRead->isAvailable(DXL1_ID, ADDRESS_PRESENT_POSITION, LENGTH_PRESENT_POSITION);

  // Check if groupsyncread data of Dynamixel#2 is available
  dxl_getdata_result = groupSyncRead->isAvailable(DXL2_ID, ADDRESS_PRESENT_POSITION, LENGTH_PRESENT_POSITION);

  // Get Dynamixel#1 present position value
  dxl1_present_position = groupSyncRead->getData(DXL1_ID, ADDRESS_PRESENT_POSITION, LENGTH_PRESENT_POSITION);

  // Get Dynamixel#2 present position value
  dxl2_present_position = groupSyncRead->getData(DXL2_ID, ADDRESS_PRESENT_POSITION, LENGTH_PRESENT_POSITION);

}
